﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StructExample
{
    class Program
    {
        static void Main(string[] args)
        {
            // structs dont need to use new statement
            CoOrds coords1;
            coords1.x = 10;
            coords1.y = 20;

            Console.Write("CoOrds 1: ");
            Console.WriteLine("x = {0}, y = {1}", coords1.x, coords1.y);

            // can use new statement
            CoOrds coords2 = new CoOrds();
            CoOrds coords3 = new CoOrds(10, 10);

            Console.Write("Coords 2: ");
            Console.WriteLine("x = {0}, y = {1}", coords2.x, coords2.y);

            Console.Write("Coords 3: ");
            Console.WriteLine("x = {0}, y = {1}", coords3.x, coords3.y);
            
            Console.WriteLine("Press any key to exit.");
            Console.ReadKey();
        }
    }
}
